class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.integer :user_id
      t.string :name
      t.string :surname
      t.integer :pion
      t.date :joined
      t.string :town
      t.string :about
      t.string :hobbies
      t.integer :points_all
      t.integer :points
      t.boolean :krzyz
      t.boolean :naramiennik
      t.integer :stop_instr
      t.integer :stop_harc

      t.timestamps
    end
  end
end
