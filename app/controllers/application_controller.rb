class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :have_char
  private
  	def have_char
  		if user_signed_in?
  			if action_name!='destroy'
	  			if controller_name!= 'characters' && controller_name!='devise/sessions'
	  				if current_user.character.nil?
	  					redirect_to new_character_path
	  				end
	  			end
	  		end
  		end
  	end
end
