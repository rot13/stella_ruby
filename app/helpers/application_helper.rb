module ApplicationHelper
	def logon_text
		if current_user.character.nil?
			current_user.email
		else
			current_user.character.name + ' ' + current_user.character.surname
		end
	end
end
