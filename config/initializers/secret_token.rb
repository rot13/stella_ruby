# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
# Stella::Application.config.secret_key_base = 'e7ae27a7a73c1aaaaf6ab7225a9d9f70b92a745a5af58354ecd2370d8f14fb1f875787169738c272b67a6fda6bbc0479eccf4de770a2c30ddda5f08e8e5d92dc'

require 'securerandom'

def secure_token
  token_file = Rails.root.join('.secret')
  if File.exist?(token_file)
    # Use the existing token.
    File.read(token_file).chomp
  else
    # Generate a new token and store it in token_file.
    token = SecureRandom.hex(64)
    File.write(token_file, token)
    token
  end
end

Stella::Application.config.secret_key_base = secure_token