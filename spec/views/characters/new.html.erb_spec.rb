require 'spec_helper'

describe "characters/new" do
  before(:each) do
    assign(:character, stub_model(Character,
      :user_id => 1,
      :name => "MyString",
      :surname => "MyString",
      :pion => 1,
      :town => "MyString",
      :about => "MyString",
      :hobbies => "MyString",
      :points_all => 1,
      :points => 1,
      :krzyz => false,
      :naramiennik => false,
      :stop_instr => 1,
      :stop_harc => 1
    ).as_new_record)
  end

  it "renders new character form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", characters_path, "post" do
      assert_select "input#character_user_id[name=?]", "character[user_id]"
      assert_select "input#character_name[name=?]", "character[name]"
      assert_select "input#character_surname[name=?]", "character[surname]"
      assert_select "input#character_pion[name=?]", "character[pion]"
      assert_select "input#character_town[name=?]", "character[town]"
      assert_select "input#character_about[name=?]", "character[about]"
      assert_select "input#character_hobbies[name=?]", "character[hobbies]"
      assert_select "input#character_points_all[name=?]", "character[points_all]"
      assert_select "input#character_points[name=?]", "character[points]"
      assert_select "input#character_krzyz[name=?]", "character[krzyz]"
      assert_select "input#character_naramiennik[name=?]", "character[naramiennik]"
      assert_select "input#character_stop_instr[name=?]", "character[stop_instr]"
      assert_select "input#character_stop_harc[name=?]", "character[stop_harc]"
    end
  end
end
