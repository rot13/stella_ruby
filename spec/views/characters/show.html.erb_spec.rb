require 'spec_helper'

describe "characters/show" do
  before(:each) do
    @character = assign(:character, stub_model(Character,
      :user_id => 1,
      :name => "Name",
      :surname => "Surname",
      :pion => 2,
      :town => "Town",
      :about => "About",
      :hobbies => "Hobbies",
      :points_all => 3,
      :points => 4,
      :krzyz => false,
      :naramiennik => false,
      :stop_instr => 5,
      :stop_harc => 6
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/Surname/)
    rendered.should match(/2/)
    rendered.should match(/Town/)
    rendered.should match(/About/)
    rendered.should match(/Hobbies/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/5/)
    rendered.should match(/6/)
  end
end
