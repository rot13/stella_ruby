require 'spec_helper'

describe "characters/index" do
  before(:each) do
    assign(:characters, [
      stub_model(Character,
        :user_id => 1,
        :name => "Name",
        :surname => "Surname",
        :pion => 2,
        :town => "Town",
        :about => "About",
        :hobbies => "Hobbies",
        :points_all => 3,
        :points => 4,
        :krzyz => false,
        :naramiennik => false,
        :stop_instr => 5,
        :stop_harc => 6
      ),
      stub_model(Character,
        :user_id => 1,
        :name => "Name",
        :surname => "Surname",
        :pion => 2,
        :town => "Town",
        :about => "About",
        :hobbies => "Hobbies",
        :points_all => 3,
        :points => 4,
        :krzyz => false,
        :naramiennik => false,
        :stop_instr => 5,
        :stop_harc => 6
      )
    ])
  end

  it "renders a list of characters" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Surname".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Town".to_s, :count => 2
    assert_select "tr>td", :text => "About".to_s, :count => 2
    assert_select "tr>td", :text => "Hobbies".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
  end
end
